﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

[CreateAssetMenu()]
public class TileInfo : ScriptableObject
{
    [SerializeField] Tile referenceTile;
    [SerializeField] bool isWalkable;

    public Tile ReferenceTile { get { return referenceTile; } }
    public bool IsWalkable { get { return isWalkable; } }
}
