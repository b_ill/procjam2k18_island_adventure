﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MapGenParams
{
    public int seed;
    public bool useSeed;
    public bool enableLogging;

    [Header("Size")]
    public int width;
    public int height;

    [Header("Shape Filtering")]
    public float primaryAmplitude; 
    public float secondaryAmplitude;
    public float primaryPeriod;
    public float secondaryPeriod;
    public bool filterBeforeCA;
    public bool filterAfterCA;
    [Range(0,100)]
    public float radius;
    [Range(0f, Mathf.PI * 2f)]
    public float startingOffset;


    [Header("Cellular Automata")]

    [Tooltip("The initial chance that a tile will be water")]
    [Range(0f,1.0f)]
    public float initialChance;

    [Tooltip("Number of iterations to apply rules")]
    [Range(0, 20)]
    public int numCAIterations;

    [Tooltip("If the number of cells in a 3x3 grid centered on the tile are at least this number, the center will be water")]
    [Range(0, 9)]
    public int neighborThreshold;

    [Range(0, 10)]
    public int numBorderTiles;


    [Header("Beaches")]
    [Tooltip("Number of times to iterate sand rules")]
    [Range(0, 10)]
    public int numSandIterations;

    [Header("Features")]
    public float perlinMultiplier;
    [Range(0f, 1f)]
    public float treeThreshold;
    [Range(0, 20)]
    public int treasureThreshold;
}
