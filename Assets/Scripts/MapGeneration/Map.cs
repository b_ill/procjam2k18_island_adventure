﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Map
{
    public Vector2Int startPosition;
    public TerrainTypes[,] terrain;
    public FeatureTypes[,] features;
    public bool[,] fog;

    public bool IsPathable(Vector2Int pos)
    {
        return IsPathable(pos.x, pos.y);
    }

    public bool IsPathable(int x, int y)
    {
        bool isTerrainPassable = Utils.DoesMapContainPoint(terrain, x, y) && (terrain[x, y] == TerrainTypes.Grass || terrain[x, y] == TerrainTypes.Sand);
        bool isFeaturePassable = Utils.DoesMapContainPoint(features, x, y) && (features[x, y] == FeatureTypes.None || features[x, y] == FeatureTypes.Treasure);
        
        return isTerrainPassable && isFeaturePassable;
    }

    public void UpdateFog(Vector2Int playerPosition, int radius)
    {
        int x = playerPosition.x;
        int y = playerPosition.y;
        for (int localX = -radius; localX < radius; localX++)
        {
            for (int localY = -radius; localY < radius; localY++)
            {
                if (Utils.DoesMapContainPoint(fog, x + localX, y + localY))
                {
                    fog[x + localX, y + localY] = false;
                }
            }
        }
    }
}
