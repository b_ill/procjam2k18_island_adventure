﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MapGeneratorController : MonoBehaviour
{
    [SerializeField] MapGenParams mapGenParams;

    [Header("Components")]
    [SerializeField] Button generateButton;
    [SerializeField] MapDrawer drawer;

    private void Start()
    {
        generateButton.onClick.AddListener(Generate);
    }

    void Generate()
    {
        Debug.Log("Clicked");
        var terrain = MapGenerator.GenerateTerrain(mapGenParams);
        var features = FeatureGenerator.GenerateFeatures(mapGenParams, terrain);
        var fog = new bool[terrain.GetLength(0), terrain.GetLength(1)];
        for(int x = 0; x < terrain.GetLength(0); x++)
        {
            for(int y = 0; y < terrain.GetLength(1); y++)
            {
                fog[x, y] = true;
            }
        }

        var map = new Map();
        map.terrain = terrain;
        map.features = features;
        map.fog = fog;


        while (map.startPosition == Vector2Int.zero)
        {
            int x = Random.Range(0, map.terrain.GetLength(0));
            int y = Random.Range(0, map.terrain.GetLength(1));
            if(map.terrain[x,y] == TerrainTypes.Sand)
            {
                map.startPosition = new Vector2Int(x, y);
                break;
            }
        }

        World.Map = map;

        drawer.DrawMap(map);
    }
}
