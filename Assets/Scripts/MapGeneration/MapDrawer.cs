﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class MapDrawer : MonoBehaviour {

    [SerializeField] Tilemap backgroundMap;
    [SerializeField] Tilemap foregroundMap;
    [SerializeField] Tilemap fogMap;

    [SerializeField] TileInfo waterTileInfo;
    [SerializeField] TileInfo groundTileInfo;
    [SerializeField] TileInfo sandTileInfo;
    [SerializeField] Tile defaultTile;
    [SerializeField] Tile treeTile;
    [SerializeField] Tile treasureTile;
    [SerializeField] Tile fogTile;

    public void DrawMap(Map map)
    {
        DrawTerrain(map.terrain);
        DrawFeatures(map.features);
        DrawFog(map.fog);
    }

    public void DrawTerrain(TerrainTypes[,] terrain)
    {
        backgroundMap.ClearAllTiles();

        int width = terrain.GetLength(0);
        int height = terrain.GetLength(1);
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                backgroundMap.SetTile(new Vector3Int(x, y, 0), GetTileForId(terrain[x, y]));
            }
        }
    }

    public void DrawFeatures(FeatureTypes[,] featureMap)
    {
        foregroundMap.ClearAllTiles();

        int width = featureMap.GetLength(0);
        int height = featureMap.GetLength(1);
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                if (featureMap[x, y] != FeatureTypes.None)
                {
                    foregroundMap.SetTile(new Vector3Int(x, y, 0), GetTileForId(featureMap[x, y]));
                }
            }
        }
    }

    public void DrawFog(bool[,] fog)
    {
        Debug.Log("Drawing fog");
        fogMap.ClearAllTiles();

        int width = fog.GetLength(0);
        int height = fog.GetLength(1);
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                if (fog[x, y] == true)
                {
                    fogMap.SetTile(new Vector3Int(x, y, 0), fogTile);
                }
                else
                {
                    fogMap.SetTile(new Vector3Int(x, y, 0), null);
                }
            }
        }
    }

    Tile GetTileForId(TerrainTypes id)
    {
        switch(id)
        {
            case TerrainTypes.Water:
                return waterTileInfo.ReferenceTile;
            case TerrainTypes.Grass:
                return groundTileInfo.ReferenceTile;
            case TerrainTypes.Sand:
                return sandTileInfo.ReferenceTile;
            default:
                return defaultTile;

        }
    }

    Tile GetTileForId(FeatureTypes id)
    {
        switch (id)
        {
            case FeatureTypes.Tree:
                return treeTile;
            case FeatureTypes.Treasure:
                return treasureTile;
            default:
                return defaultTile;

        }
    }
}
