﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGenerator
{
    public static TerrainTypes[,] GenerateTerrain(MapGenParams mapGenParams)
    {
        TerrainTypes[,] map = new TerrainTypes[mapGenParams.width, mapGenParams.height];

        if(mapGenParams.useSeed)
        {
            Random.InitState(mapGenParams.seed);
        }

        // 1. Create a randomly populated map
        map = GenerateRandomMap(map, mapGenParams);

        // 2. Apply distance filtering before CA
        if (mapGenParams.filterBeforeCA)
            map = ApplyDistanceFiltering(map, mapGenParams);

        // 3. 
        map = GenerateCellularAutomataMap(map, mapGenParams);

        // 4. Apply distance filtering after CA
        if (mapGenParams.filterAfterCA)
            map = ApplyDistanceFiltering(map, mapGenParams);

        // 5. Create beaches
        map = CreateBeaches(map, mapGenParams);

        return map;
    }

    static TerrainTypes[,] GenerateRandomMap(TerrainTypes[,] map, MapGenParams mapGenParams)
    {
        // Initial random fill of map
        for (int x = 0; x < mapGenParams.width; x++)
        {
            for (int y = 0; y < mapGenParams.width; y++)
            {
                if (x < mapGenParams.numBorderTiles || x >= mapGenParams.width - mapGenParams.numBorderTiles
                    || y < mapGenParams.numBorderTiles || y >= mapGenParams.height - mapGenParams.numBorderTiles)
                {
                    map[x, y] = TerrainTypes.Water;
                }
                else
                {
                    map[x, y] = Random.Range(0f, 1f) < mapGenParams.initialChance ? TerrainTypes.Water : TerrainTypes.Grass;
                }
            }
        }

        return map;
    }


    static TerrainTypes[,] GenerateRandomMapFromPerlin(TerrainTypes[,] map, MapGenParams mapGenParams)
    {
        var offset = Random.Range(0f, 1f);

        // Initial random fill of map
        for (int x = 0; x < mapGenParams.width; x++)
        {
            for (int y = 0; y < mapGenParams.width; y++)
            {
                if (x < mapGenParams.numBorderTiles || x >= mapGenParams.width - mapGenParams.numBorderTiles
                    || y < mapGenParams.numBorderTiles || y >= mapGenParams.height - mapGenParams.numBorderTiles)
                {
                    map[x, y] = TerrainTypes.Water;
                }
                else
                {
                    var noise = Mathf.PerlinNoise(mapGenParams.perlinMultiplier * ((float)x / mapGenParams.width + offset),
                        mapGenParams.perlinMultiplier * ((float)y / mapGenParams.height + offset));

                    map[x, y] = noise > mapGenParams.initialChance ? TerrainTypes.Water : TerrainTypes.Grass;
                }
            }
        }

        return map;
    }

    static TerrainTypes[,] GenerateCellularAutomataMap(TerrainTypes[,] map, MapGenParams mapGenParams)
    {


        if (mapGenParams.filterBeforeCA)
        {
            map = ApplyDistanceFiltering(map, mapGenParams);
        }

        for (int it = 0; it < mapGenParams.numCAIterations; it++)
        {
            var nextMap = new TerrainTypes[mapGenParams.width, mapGenParams.height];
            for (int x = 0; x < mapGenParams.width; x++)
            {
                for (int y = 0; y < mapGenParams.height; y++)
                {
                    // skip if in border
                    if (x < mapGenParams.numBorderTiles || x >= mapGenParams.width - mapGenParams.numBorderTiles
                    || y < mapGenParams.numBorderTiles || y >= mapGenParams.height - mapGenParams.numBorderTiles)
                    {
                        nextMap[x, y] = map[x, y];
                        continue;
                    }

                    // otherwise, count neighbors.
                    int numNeighbors = Utils.GetNeighborsOfType(map, x, y, TerrainTypes.Water, true);

                    if (numNeighbors >= mapGenParams.neighborThreshold)
                    {
                        nextMap[x, y] = TerrainTypes.Water;
                    }
                    else
                    {
                        nextMap[x, y] = TerrainTypes.Grass;
                    }
                }
            }
            map = nextMap;
        }

        return map;
    }

    static TerrainTypes[,] ApplyDistanceFiltering(TerrainTypes[,] map, MapGenParams mapGenParams)
    {
        return DistanceFilterPeriodicPerturbation(map, mapGenParams);
    }

    static TerrainTypes[,] DistanceFilterPeriodicPerturbation(TerrainTypes[,] map, MapGenParams mapGenParams)
    {
        var retMap = new TerrainTypes[mapGenParams.width, mapGenParams.height];
        System.Array.Copy(map, retMap, map.Length);
        int centerX = mapGenParams.width / 2;
        int centerY = mapGenParams.height / 2;


        float randomStartingOffset = Random.Range(0f, 2 * Mathf.PI);

        for (int x = 0; x < mapGenParams.width; x++)
        {
            for (int y = 0; y < mapGenParams.height; y++)
            {
                int dY = y - centerY;
                int dX = x - centerX;
                var angle = Mathf.Atan2(dY, dX) + Mathf.PI;

                
                float distance = Mathf.Sqrt(dX * dX + dY * dY);
                distance += (mapGenParams.primaryAmplitude * Mathf.Sin(mapGenParams.primaryPeriod * angle + randomStartingOffset))
                    + (mapGenParams.secondaryAmplitude * Mathf.Sin(mapGenParams.secondaryPeriod * angle + mapGenParams.startingOffset));

                if (distance > mapGenParams.radius)
                {
                    retMap[x, y] = TerrainTypes.Water;
                }
                else
                {
                    retMap[x, y] = map[x, y];
                }

            }
        }
        return retMap;
    }

    static TerrainTypes[,] CreateBeaches(TerrainTypes[,] map, MapGenParams mapGenParams)
    {
        var retMap = new TerrainTypes[mapGenParams.width, mapGenParams.height];
        System.Array.Copy(map, retMap, map.Length);

        for (int iter = 0; iter < mapGenParams.numSandIterations; iter++)
        {
            TerrainTypes[,] newMap = new TerrainTypes[mapGenParams.width, mapGenParams.height];

            for (int x = 0; x < mapGenParams.width; x++)
            {
                for (int y = 0; y < mapGenParams.height; y++)
                {
                    if (retMap[x, y] == TerrainTypes.Water)
                    {
                        continue;
                    }
                    else
                    {
                        // If any neighbors are water
                        if (Utils.GetNeighborsOfType(retMap, x, y, TerrainTypes.Water, false) > 0)
                        {
                            // Make it a beach
                            newMap[x, y] = TerrainTypes.Sand;
                        }
                        else if (Utils.GetNeighborsOfType(retMap, x, y, TerrainTypes.Sand, false) > 1 && Random.Range(0, 2) == 0)
                        {
                            newMap[x, y] = TerrainTypes.Sand;
                        }
                        else if(retMap[x,y] == TerrainTypes.Grass && Utils.GetNeighborsOfType(retMap, x, y, TerrainTypes.Grass, false) == 0)
                        {
                            newMap[x, y] = TerrainTypes.Sand;
                        }
                        else
                        {
                            newMap[x, y] = retMap[x, y];
                        }
                    }
                }
            }

            retMap = newMap;
        }

        return retMap;
    }
}
