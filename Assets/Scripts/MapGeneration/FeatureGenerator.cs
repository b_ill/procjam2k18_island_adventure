﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FeatureGenerator
{
    public static FeatureTypes[,] GenerateFeatures(MapGenParams mapGenParams, TerrainTypes[,] terrain)
    {
        FeatureTypes[,] features = new FeatureTypes[mapGenParams.width, mapGenParams.height];
        features = PlaceTrees(mapGenParams, features, terrain);
        features = PlaceTreasures(mapGenParams, features, terrain);

        return features;
    }

    static FeatureTypes[,] PlaceTrees(MapGenParams mapGenParams, FeatureTypes[,] features, TerrainTypes[,] terrain)
    {
        var retMap = new FeatureTypes[mapGenParams.width, mapGenParams.height];
        System.Array.Copy(features, retMap, features.Length);

        var offset = Random.Range(0f, 1f);

        for (int x = 0; x < mapGenParams.width; x++)
        {
            for (int y = 0; y < mapGenParams.height; y++)
            {
                var noise = Mathf.PerlinNoise(mapGenParams.perlinMultiplier * ((float)x / mapGenParams.width + offset),
                    mapGenParams.perlinMultiplier * ((float)y / mapGenParams.height + offset));

                if (noise > mapGenParams.treeThreshold && terrain[x, y] == TerrainTypes.Grass)
                {
                    retMap[x, y] = FeatureTypes.Tree;
                }
            }
        }

        return retMap;
    }

    static FeatureTypes[,] PlaceTreasures(MapGenParams mapGenParams, FeatureTypes[,] features, TerrainTypes[,] terrain)
    {
        var retMap = new FeatureTypes[mapGenParams.width, mapGenParams.height];
        System.Array.Copy(features, retMap, features.Length);

        int numTreasures = 0;

        for (int x = 0; x < mapGenParams.width; x++)
        {
            for (int y = 0; y < mapGenParams.height; y++)
            {
                if(retMap[x,y] == FeatureTypes.None 
                   && terrain[x,y] != TerrainTypes.Water
                   && Utils.GetNeighborsOfType(features, x, y, FeatureTypes.Tree, true, 2) >= mapGenParams.treasureThreshold)
                {
                    retMap[x, y] = FeatureTypes.Treasure;
                    numTreasures++;
                }
            }
        }

        Debug.Log("Treasures: " + numTreasures.ToString());
        return retMap;
    }
}