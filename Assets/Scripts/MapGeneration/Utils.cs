﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Utils : MonoBehaviour
{
   public static int GetNeighborsOfType(TerrainTypes[,] map, int posX, int posY, TerrainTypes type, bool includeSelf, int numSteps = 1)
    {
        int numNeighbors = 0;
        for (int x = posX - numSteps; x <= posX + numSteps; x++)
        {
            for (int y = posY - numSteps; y <= posY + numSteps; y++)
            {
                if (x == 0 && y == 0 && !includeSelf) continue;

                if (DoesMapContainPoint(map, x, y))
                {
                    if (map[x, y] == type) numNeighbors++;
                }
                else
                {
                    continue;
                }
            }
        }

        return numNeighbors;
    }

    public static int GetNeighborsOfType(FeatureTypes[,] map, int posX, int posY, FeatureTypes type, bool includeSelf = true, int numSteps = 1)
    {
        int numNeighbors = 0;
        for (int x = posX - numSteps; x <= posX + numSteps; x++)
        {
            for (int y = posY - numSteps; y <= posY + numSteps; y++)
            {
                if (x == 0 && y == 0 && !includeSelf) continue;

                if (DoesMapContainPoint(map, x, y))
                {
                    if (map[x, y] == type) numNeighbors++;
                }
                else
                {
                    continue;
                }
            }
        }

        return numNeighbors;
    }

    public static bool DoesMapContainPoint(bool[,] map, int posX, int posY)
    {
        if (posX < 0 || posX >= map.GetLength(0)
            || posY < 0 || posY >= map.GetLength(1))
        {
            return false;
        }
        return true;
    }

    public static bool DoesMapContainPoint(TerrainTypes[,] map, int posX, int posY)
    {
        if (posX < 0 || posX >= map.GetLength(0)
            || posY < 0 || posY >= map.GetLength(1))
        {
            return false;
        }
        return true;
    }

    public static bool DoesMapContainPoint(FeatureTypes[,] map, int posX, int posY)
    {
        if (posX < 0 || posX >= map.GetLength(0)
            || posY < 0 || posY >= map.GetLength(1))
        {
            return false;
        }
        return true;
    }
}
