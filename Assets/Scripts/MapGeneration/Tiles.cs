﻿public enum TerrainTypes
{
    Water = 0,
    Grass = 1,
    Sand = 2,
}

public enum FeatureTypes
{
    None = 0,
    Tree = 1,
    Treasure = 2,
}