﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStateManager : MonoBehaviour
{

    [SerializeField] GameObject[] gameplay;
    [SerializeField] GameObject[] mapGen;

    

    public enum States
    {
        None,
        MapGen,
        Play
    }

    States state = States.MapGen;

    public void Play()
    {
        foreach (GameObject obj in mapGen)
        {
            obj.SetActive(false);
        }
        foreach (GameObject obj in gameplay)
        {
            obj.SetActive(true);
        }
    }
}