﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerView : MonoBehaviour
{
    public void UpdatePosition(Vector2Int newPosition)
    {
        transform.position = new Vector3(newPosition.x, newPosition.y);
    }
}
