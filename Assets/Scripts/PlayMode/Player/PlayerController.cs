﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// For now, this is also going to track the player position
public class PlayerController : MonoBehaviour
{

    [SerializeField] PlayerView view;
    [SerializeField] World world;

    public Vector2Int position;
    public int visibilityRadius;

    private void Start()
    {
        position = World.Map.startPosition;
        view.UpdatePosition(position);
        world.UpdateFog();
    }


    private void Update()
    {
        Vector2Int move = Vector2Int.zero;

        if(Input.GetKeyDown(KeyCode.LeftArrow))
        {
            move += Vector2Int.left;
        }
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            move += Vector2Int.right;
        }
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            move += Vector2Int.up;
        }
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            move += Vector2Int.down;
        }

        Vector2Int nextPosition = position + move;

        if(nextPosition != position)
        {
            Debug.Log("Trying to move to " + position.ToString());
            position = world.TryMoveToPosition(nextPosition, position);
            view.UpdatePosition(position);
        }
    }
}
