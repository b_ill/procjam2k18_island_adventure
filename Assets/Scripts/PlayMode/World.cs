﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class World : MonoBehaviour
{
    public static Map Map { get; set; }

    [SerializeField] PlayerController player;
    [SerializeField] MapDrawer mapDrawer;

    public Vector2Int TryMoveToPosition(Vector2Int intendedPosition, Vector2Int currentPosition)
    {
        if (Map.IsPathable(intendedPosition))
        {
            UpdateFog();
            return intendedPosition;
        }
        else
        {
            return currentPosition;
        }
    }

    public void UpdateFog()
    {
        Map.UpdateFog(player.position, player.visibilityRadius);
        mapDrawer.DrawFog(Map.fog);
    }
}
